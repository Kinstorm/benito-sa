<?php

require("fpdf181/fpdf.php");

//extendemos CI_Controller
class main_controller extends CI_Controller {

    public function __construct() {
        //llamamos al constructor de la clase padre
        parent::__construct();

        //llamo al helper url
        $this->load->helper("url");

        //llamo o incluyo el modelo
        $this->load->model("ruta_model");
        $this->load->model("oferta_model");
        $this->load->model("usuario_model");
        $this->load->model("billete_model");

        //cargo la libreria de sesiones
        $this->load->library("session");
    }

    //controlador por defecto
    public function index() {
        $datos = array();

        //comprobamos si hay usuario
        if ($this->session->userdata('logueado')) {
            $datos['nombre'] = $this->session->userdata('nombre');
            $datos['tipo'] = $this->session->userdata('tipo');

            if ($datos['tipo'] == "admin") {
                $this->load->view("admin_view", $datos);
            } else {
                //listamos las rutas
                $datos["listarRuta"] = $this->ruta_model->listarRutaPaginaPrincipal();

                //listamos las ofertas
                $datos["listarOferta"] = $this->oferta_model->listarOferta();

                //listamos las ofertas
                //Independientemente de que haya o no usuario mostramos la pantalla principal
                $this->load->view("main_view", $datos);
            }
        } else {
            //listamos las rutas
            $datos["listarRuta"] = $this->ruta_model->listarRutaPaginaPrincipal();

            //listamos las ofertas
            $datos["listarOferta"] = $this->oferta_model->listarOferta();

            $this->load->view("main_view", $datos);
        }
    }

    public function detalleRuta($idruta) {

        if (is_numeric($idruta)) {

            $datos["ruta_detalle"] = $this->ruta_model->obtenerRuta($idruta);
            $datos["parada_detalle"] = $this->ruta_model->obtenerParadasRuta($idruta);

            $this->load->view("ruta_detalle_view", $datos);
        }
    }

    public function buscarRuta() {

        //compruebo si se ha enviado submit
        if ($this->input->post("submit")) {

            //recuperamos los datos obtenidos en el formulario
            $datos['origen'] = $this->input->post("origen");
            $datos['destino'] = $this->input->post("destino");
            $datos['npasajeros'] = $this->input->post("npasajeros");

            $this->load->view("client_listado_rutas", $datos);
            //
        } else {
            
        }
    }

    public function detalleOferta($idoferta) {

        if (is_numeric($idoferta)) {

            $datos["oferta_detalle"] = $this->oferta_model->obtenerOferta($idoferta);

            $this->load->view("oferta_detalle_view", $datos);
        }
    }

    public function filtrado() {

        if ($this->input->post("submit")) {
            $origen = $this->input->post("origen");
            $destino = $this->input->post("destino");
            $pasajeros = $this->input->post("pasajeros");

            //guardamos los datos recibidos
            $this->session->set_flashdata('origen', $origen);
            $this->session->set_flashdata('destino', $destino);
            $this->session->set_flashdata('pasajeros', $pasajeros);


//comprobamos si hay usuario
            if ($this->session->userdata('logueado')) {
                $datos['nombre'] = $this->session->userdata('nombre');
                $datos['tipo'] = $this->session->userdata('tipo');
            } else {
                // redirect('main_controller/iniciar_sesion');
            }

            //listamos las rutas
            $datos["listarRuta"] = $this->ruta_model->listarRutaFiltro($origen, $destino);

            //listamos las ofertas
            $datos["listarOferta"] = $this->oferta_model->listarOfertaFiltro($origen, $destino);

            $datos["origen"] = $origen;
            $datos["destino"] = $destino;
            $datos["pasajeros"] = $pasajeros;

            debug_to_console("numero de pasajeros" . $pasajeros);

            $this->load->view("main_view", $datos);
        } else {
            $this->index();
        }
    }

    function Login() {
        if ($this->input->post("submit")) {
            $correo = $this->input->post("correo");
            $pass = $this->input->post("pass");

            //comprobamos si el usuario existe

            if ($this->usuario_model->autentificarUsuario($origen, $destino)) {
                //obtenemos usuario
                $usuarioActivo = $this->usuario_model->obtenerUsuarioCorreo($correo);



                $this->load->view("main_view");
            } else {
                //mostrar error
            }
        } else {
            
        }
    }

    public function iniciarSesion() {

        $datos = array();
        $datos['errorLogin'] = $this->session->flashdata('errorLogin');

        debug_to_console("pantalla iniciar sesion" . $this->session->flashdata('errorLogin'));
        $this->load->view("user_login_view", $datos);
    }

    public function iniciarSesionPost() {

        if ($this->input->post("submit")) {

            $correo = $this->input->post('correo');
            $password = $this->input->post('password');
            debug_to_console($correo . " + " . $password);
            $usuario = $this->usuario_model->usuario_por_correo_contrasena($correo, $password);

            if ($usuario) {
                $usuario_data = array(
                    'usuarioid' => $usuario->idusuario,
                    'nombre' => $usuario->nombre,
                    'tipo' => $usuario->tipo,
                    'logueado' => TRUE
                );
                $this->session->set_userdata($usuario_data);
                // redirect('main_controller/logueado');
                $this->index();
            } else {
                debug_to_console('El correo o la contraseña son incorrectos.');
                $this->session->set_flashdata('errorLogin', 'El correo o la contraseña son incorrectos.');
                $this->iniciarSesion();
                //redirect('main_controller/iniciar_sesion');
            }
        } else {

            debug_to_console("formulario mal pillado");
            $this->iniciarSesion();
        }
    }

    /* public function logueado() {
      if ($this->session->userdata('logueado')) {
      $data = array();
      $data['nombre'] = $this->session->userdata('nombre');
      $this->load->view("logueado", $data);
      } else {
      redirect('main_controller/iniciar_sesion');
      }
      } */

    public function cerrarSesion() {
        $usuario_data = array(
            'logueado' => FALSE
        );
        $this->session->set_userdata($usuario_data);
        $this->index();
    }

    public function registro() {
        $datos = array();
        $datos['errorRegister'] = $this->session->flashdata('errorRegister');
        debug_to_console("pantalla registro");
        $this->load->view("user_register_view", $datos);
    }

    public function registroPost() {
        if ($this->input->post("submit")) {

            $nombre = $this->input->post('nombre');
            $apellidos = $this->input->post('apellidos');
            $correo = $this->input->post('correo');
            $password = $this->input->post('password');
            $dni = $this->input->post('dni');
            $direccion = $this->input->post('direccion');
            $telefono = $this->input->post('telefono');
            $nacionalidad = $this->input->post('nacionalidad');
            $tipo = "user";

            debug_to_console($correo);
            $usuario = $this->usuario_model->usuario_por_correo($correo);

            if ($usuario) {
                debug_to_console("correo ya existe");
                //el correo ya existe
                $this->session->set_flashdata('errorRegister', 'El correo ya esta en uso');
                $this->registro();
            } else {
                debug_to_console("correo no existe");
                //el correo no existe, creamos el usuario
                $nuevo = $this->usuario_model->nuevoUsuario($correo, $password, $nombre, $apellidos, $dni, $direccion, $telefono, $nacionalidad, $tipo);

                if ($nuevo == true) {
                    //obtenemos el usuario recien creado
                    $usuario = $this->usuario_model->usuario_por_correo_contrasena($correo, $password);
                    $usuario_data = array(
                        'usuarioid' => $usuario->idusuario,
                        'nombre' => $usuario->nombre,
                        'logueado' => TRUE
                    );
                    $this->session->set_userdata($usuario_data);
                    // redirect('main_controller/logueado');
                    $this->index();
                }
            }
        } else {

            debug_to_console("formulario mal pillado");
            $this->registro();
        }
    }

    public function cookiesView() {

        $this->load->view("cookies_view");
    }

    public function politicaView() {

        $this->load->view("politica_view");
    }

    public function misViajes() {

        $idusuario = $this->session->userdata('usuarioid');

        //listamos los billetes
        $datos["listarBilletes"] = $this->billete_model->listarBillete($idusuario);

        $this->load->view("viajes_view", $datos);
    }

    public function adquirirBillete($ruta) {

        if ($this->session->userdata('logueado')) {
            $idusuario = $this->session->userdata('usuarioid');
            $datos["usuario"] = $this->usuario_model->obtenerUsuario($idusuario);
            $datos["rutaDetalle"] = $this->ruta_model->obtenerRuta($ruta);
            $datos["rutaParadas"] = $this->ruta_model->obtenerParadasRuta($ruta);

            if ($this->oferta_model->obtenerOferta($ruta)) {
                $datos["oferta"] = $this->oferta_model->obtenerOfertaRuta($ruta);
            }

            $this->load->view("compra_view", $datos);
        } else {
            $this->iniciarSesion();
        }
    }

    public function adquirirBilleteCompra($idruta) {

        $idusuario = $this->session->userdata('usuarioid');
        $rutaDetalle = $this->ruta_model->obtenerRuta($idruta);
        $rutaOrigen = $this->ruta_model->obtenerRutaOrigen($idruta);
        $datos["usuario"] = $this->usuario_model->obtenerUsuario($idusuario);


        $localizador = generateRandomString();
        $asiento = $rutaDetalle[0]->plazas - 1;
        $precio = 5;
        $fecha = date('Y-m-d');
        $hora = $rutaOrigen[0]->hora;

        $nuevo = $this->billete_model->nuevoBillete($localizador, $asiento, $precio, $fecha, $hora, $idusuario, $idruta);
        $datos["billete"] = $this->billete_model->obtenerBilleteLocalizador($localizador);

        if ($nuevo == true) {
            $this->ruta_model->actualizarPlazas($idruta, $asiento);

            $this->load->view("post_compra_view", $datos);
        } else {
            echo "fallo";
        }
    }

    public function imprimirBillete($idbillete,$ruta) {

        $idusuario = $this->session->userdata('usuarioid');
        $usuario = $this->usuario_model->obtenerUsuario($idusuario);
        $ruta = $this->ruta_model->obtenerRutaOrigenDestino($ruta);
        $billete = $this->billete_model->obtenerBillete($idbillete);

        $pdf = new PDF();
        $pdf->AliasNbPages();
        $pdf->AddPage();
        $pdf->SetFont('Times', '', 12);
        $pdf->Cell(0, 10, 'Nombre: ' . $usuario[0]->nombre, 0, 1);
        $pdf->Cell(0, 10, 'Apellidos: ' . $usuario[0]->apellidos, 0, 1);
        $pdf->Cell(0, 10, 'Nif: ' . $usuario[0]->dni, 0, 1);
        $pdf->Cell(0, 10, 'Correo: ' . $usuario[0]->email, 0, 1);
        $pdf->Cell(0, 10, 'Localizador: ' . $billete[0]->localizador, 0, 1);
        $pdf->Cell(0, 10, 'Ruta: ' . $ruta[0]->ruta, 0, 1);
        $pdf->Cell(0, 10, 'Origen: ' . $ruta[0]->origen, 0, 1);
        $pdf->Cell(0, 10, 'Destino: ' . $ruta[0]->destino, 0, 1);
        $pdf->Cell(0, 10, 'Paradas: ' . $ruta[0]->paradas, 0, 1);

        $pdf->Output();
        
    }

}

function debug_to_console($data) {
    $output = $data;
    if (is_array($output))
        $output = implode(',', $output);

    echo "<script>console.log( 'Debug Objects: " . $output . "' );</script>";
}

function generateRandomString($length = 5) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

class PDF extends FPDF {

// Cabecera de página
    function Header() {
        // Logo
        $this->Image(base_url('../images/benitosa.png'), 20, 16, 45);
        // Arial bold 15
        $this->SetFont('Arial', 'B', 15);
        // Movernos a la derecha
        $this->Cell(80);
        // Título
        $this->Cell(50, 10, 'Billete de Bus', 1, 0, 'C');
        // Salto de línea
        $this->Ln(20);
    }

// Pie de página
    function Footer() {
        // Posición: a 1,5 cm del final
        $this->SetY(-15);
        // Arial italic 8
        $this->SetFont('Arial', 'I', 8);
        // Número de página
        $this->Cell(0, 10, 'Page ' . $this->PageNo() . '/{nb}', 0, 0, 'C');
    }

}

?>