<?php

//extendemos CI_Model
class parada_model extends CI_Model {

    public function __construct() {
        //llamamos al constructor de la clase padre
        parent::__construct();

        //cargamos la base de datos
        $this->load->database();
    }

    public function nuevaParada($localidad, $direccion, $provincia, $codigo_postal) {
        $consulta = $this->db->query("INSERT INTO parada VALUES('$localidad','$direccion','$provincia', '$codigo_postal');");
        if ($consulta == true) {
            return true;
        } else {
            return false;
        }
    }

    public function obtenerParada($idparada) {

        //Comprobamos si la parada existe
        $consulta = $this->db->query("SELECT * FROM parada WHERE parada.$idparada = '$idparada'");
        if ($consulta->num_rows() != 0) {
            //obtenemos la parada y info relativa
            $consulta = $this->db->query("SELECT * FROM parada WHERE  parada.$idparada = '$idparada';");

            //Devolvemos el resultado de la consulta
            return $consulta->result();
        } else {
            return false;
        }
    }

    public function actualizarParada($idparada, $localidad, $direccion, $provincia, $codigo_postal) {
        //Comprobamos si la parada existe
        $consulta = $this->db->query("SELECT * FROM parada WHERE parada.idparada = '$idparada'");
        if ($consulta->num_rows() != 0) {
            //actualizamos la parada
            $consulta = $this->db->query("UPDATE parada SET localidad = '$localidad', direccion = '$direccion', provincia = '$provincia', "
                    . "codigo_postal = '$codigo_postal' WHERE (`idparada` = '$idparada');");
            if ($consulta == true) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
    
    public function eliminarParada($idparada) {
        $consulta = $this->db->query("DELETE FROM parada WHERE $idparada=$idparada");
        if ($consulta == true) {
            return true;
        } else {
            return false;
        }
    }

    public function listarParada() {
        //Hacemos una consulta
        $consulta = $this->db->query("SELECT * FROM parada;");

        //Devolvemos el resultado de la consulta
        return $consulta->result();
    }


}

?>