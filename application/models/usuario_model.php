<?php

//extendemos CI_Model
class usuario_model extends CI_Model {

    public function __construct() {
        //llamamos al constructor de la clase padre
        parent::__construct();

        //cargamos la base de datos
        $this->load->database();
    }

    public function nuevoUsuario($email, $password, $nombre, $apellidos, $dni, $direccion, $telefono, $nacionalidad, $tipo) {
        $consulta = $this->db->query("INSERT INTO usuario VALUES(null, '$email','$password','$nombre','$apellidos','$dni','$direccion', $telefono, '$nacionalidad', '$tipo');");
        if ($consulta == true) {
            return true;
        } else {
            return false;
        }
    }

    public function obtenerUsuario($idusuario) {

        //Comprobamos si el usuario existe
        $consulta = $this->db->query("SELECT * FROM usuario WHERE usuario.idusuario = '$idusuario'");
        if ($consulta->num_rows() != 0) {
            //obtenemos el usuario y info relativa
            $consulta = $this->db->query("SELECT * FROM usuario WHERE usuario.idusuario = '$idusuario';");

            //Devolvemos el resultado de la consulta
            return $consulta->result();
        } else {
            return false;
        }
    }

    public function usuario_por_correo_contrasena($correo, $password) {
        $this->db->select('idusuario, nombre, tipo');
        $this->db->from('usuario');
        $this->db->where('email', $correo);
        $this->db->where('password', $password);
        $consulta = $this->db->get();
        $resultado = $consulta->row();
        return $resultado;
    }

    public function usuario_por_correo($correo) {
        $this->db->select('idusuario, nombre');
        $this->db->from('usuario');
        $this->db->where('email', $correo);
        $consulta = $this->db->get();
        $resultado = $consulta->row();
        return $resultado;
    }

    public function actualizarUsuario($idusuario, $email, $password, $nombre, $apellidos, $dni, $direccion, $telefono, $nacionalidad, $tipo) {
        //Comprobamos si el usuario existe
        $consulta = $this->db->query("SELECT * FROM usuario WHERE usuario.idusuario = '$idusuario';");
        if ($consulta->num_rows() != 0) {
            //actualizamos el usuario
            $consulta = $this->db->query("UPDATE usuario SET email = '$email', password = '$password', nombre = '$nombre', apellidos = '$apellidos', "
                    . "dni = '$dni', direccion = '$direccion', telefono = '$telefono', nacionalidad = '$nacionalidad', tipo = '$tipo'  WHERE (`idusuario` = '$idusuario');");
            if ($consulta == true) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function autentificarUsuario($email, $password) {
        //comprobamos si el usuario existe
        $consulta = $this->db->query("SELECT * FROM usuario WHERE usuario.email = '$email' AND usuario.password = '$password'");
        if ($consulta->num_rows() != 0) {
            return true;
        } else {
            return false;
        }
    }

    public function eliminarUsuario($idusuario) {
        $consulta = $this->db->query("DELETE FROM usuario WHERE idusuario=$idusuario");
        if ($consulta == true) {
            return true;
        } else {
            return false;
        }
    }

}

?>