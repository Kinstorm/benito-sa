<?php

//extendemos CI_Model
class detencion_model extends CI_Model {

    public function __construct() {
        //llamamos al constructor de la clase padre
        parent::__construct();

        //cargamos la base de datos
        $this->load->database();
    }

    public function nuevaDetencion($duracion, $tipo_parada, $hora, $orden, $ruta_id, $parada_id) {
        $consulta = $this->db->query("INSERT INTO detencion VALUES('$duracion', '$tipo_parada', '$hora', '$orden', '$ruta_id', '$parada_id');");
        if ($consulta == true) {
            return true;
        } else {
            return false;
        }
    }

    public function obtenerDetencion($iddetencion) {

        //Comprobamos si la detencion existe
        $consulta = $this->db->query("SELECT * FROM detencion WHERE detencion.iddetencion = '$iddetencion'");
        if ($consulta->num_rows() != 0) {
            //obtenemos la detencion y info relativa
            $consulta = $this->db->query("SELECT * FROM detencion WHERE  detencion.iddetencion = '$iddetencion';");

            //Devolvemos el resultado de la consulta
            return $consulta->result();
        } else {
            return false;
        }
    }

    public function actualizarDetencion($iddetencion, $duracion, $tipo_parada, $hora, $orden, $ruta_id, $parada_id) {
        //Comprobamos si la detencion existe
        $consulta = $this->db->query("SELECT * FROM detencion WHERE detencion.iddetencion = '$iddetencion'");
        if ($consulta->num_rows() != 0) {
            //actualizamos la detencion
            $consulta = $this->db->query("UPDATE detencion SET duracion = '$duracion', tipo_parada = '$tipo_parada', hora = '$hora', "
                    . "orden = '$orden', ruta_id = '$ruta_id', parada_id = '$parada_id'  WHERE (`iddetencion` = '$iddetencion');");
            if ($consulta == true) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }


    public function eliminarDetencion($iddetencion) {
        $consulta = $this->db->query("DELETE FROM detencion WHERE iddetencion=$iddetencion");
        if ($consulta == true) {
            return true;
        } else {
            return false;
        }
    }

    public function listarDetencion() {
        //Hacemos una consulta
        $consulta = $this->db->query("SELECT * FROM detencion;");

        //Devolvemos el resultado de la consulta
        return $consulta->result();
    }


}

?>