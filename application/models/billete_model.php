<?php

//extendemos CI_Model
class billete_model extends CI_Model {

    public function __construct() {
        //llamamos al constructor de la clase padre
        parent::__construct();

        //cargamos la base de datos
        $this->load->database();
    }

    public function nuevoBillete($localizador, $asiento, $precio, $fecha, $hora, $usuario_id, $ruta_id) {
        $consulta = $this->db->query("INSERT INTO billete VALUES(null, '$localizador',$asiento,$precio, '$fecha', '$hora', $usuario_id, $ruta_id);");
        if ($consulta == true) {
            return true;
        } else {
            return false;
        }
    }

    public function obtenerBillete($idbillete) {

        //Comprobamos si la billete existe
        $consulta = $this->db->query("SELECT * FROM billete WHERE billete.idbillete = '$idbillete'");
        if ($consulta->num_rows() != 0) {
            //obtenemos la billete y info relativa
            $consulta = $this->db->query("SELECT * FROM billete WHERE billete.idbillete = '$idbillete'");

            //Devolvemos el resultado de la consulta
            return $consulta->result();
        } else {
            return false;
        }
    }
    
        public function obtenerBilleteLocalizador($localizador) {

        //Comprobamos si la billete existe
        $consulta = $this->db->query("SELECT * FROM billete WHERE billete.localizador = '$localizador'");
        if ($consulta->num_rows() != 0) {
            //obtenemos la billete y info relativa
            $consulta = $this->db->query("SELECT * FROM billete WHERE billete.localizador = '$localizador'");

            //Devolvemos el resultado de la consulta
            return $consulta->result();
        } else {
            return false;
        }
    }

    public function actualizarBillete($idbillete ,$localizador, $asiento, $precio, $fecha, $hora, $usuario_id, $ruta_id) {
        //Comprobamos si la billete existe
        $consulta = $this->db->query("SELECT * FROM billete WHERE billete.idbillete = '$idbillete'");
        if ($consulta->num_rows() != 0) {
            //actualizamos el billete
            $consulta = $this->db->query("UPDATE billete SET idbillete = '$idbillete', localizador = '$localizador', asiento = '$asiento', precio = '$precio', "
                    . "fecha = '$fecha', usuario_id = '$usuario_id', ruta_id = '$ruta_id', hora = '$hora' WHERE (`idbillete` = '$idbillete');");
            if ($consulta == true) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function eliminarBillete($idoferta) {
        $consulta = $this->db->query("DELETE FROM oferta WHERE idoferta=$idoferta");
        if ($consulta == true) {
            return true;
        } else {
            return false;
        }
    }

    public function listarBillete($idusuario) {
        //Hacemos una consulta
        $consulta = $this->db->query("SELECT * FROM billete, usuario WHERE billete.usuario_id = usuario.idusuario AND usuario.idusuario = '$idusuario'");

        //Devolvemos el resultado de la consulta
        return $consulta->result();
    }


}

?>