<?php

//extendemos CI_Model
class ruta_model extends CI_Model {

    public function __construct() {
        //llamamos al constructor de la clase padre
        parent::__construct();

        //cargamos la base de datos
        $this->load->database();
    }

    public function nuevaRuta($plazas, $duracion, $descripcion, $valoracion) {
        $consulta = $this->db->query("INSERT INTO ruta VALUES('$plazas','$duracion','$descripcion', '$valoracion');");
        if ($consulta == true) {
            return true;
        } else {
            return false;
        }
    }

    public function obtenerRuta($idruta) {

        //Comprobamos si la ruta existe
        $consulta = $this->db->query("SELECT * FROM ruta WHERE ruta.idruta = '$idruta'");
        if ($consulta->num_rows() != 0) {
            //obtenemos la ruta y info relativa
            /* $consulta = $this->db->query("SELECT * FROM ruta, detencion, parada WHERE ruta.idruta = detencion.ruta_id AND "
              . "parada.idparada = detencion.parada_id AND ruta.idruta = '$idruta' ORDER by detencion.orden;"); */
            $consulta = $this->db->query("SELECT * FROM ruta WHERE ruta.idruta = '$idruta'");

            //Devolvemos el resultado de la consulta
            return $consulta->result();
        } else {
            return false;
        }
    }

    public function obtenerRutaOrigen($idruta) {

        //Comprobamos si la ruta existe
        $consulta = $this->db->query("SELECT * FROM ruta, parada, detencion WHERE parada.idparada = detencion.parada_id AND detencion.ruta_id = ruta.idruta AND ruta.idruta = '$idruta' AND detencion.orden = 1");
        if ($consulta->num_rows() != 0) {
            //obtenemos la ruta y info relativa
            /* $consulta = $this->db->query("SELECT * FROM ruta, detencion, parada WHERE ruta.idruta = detencion.ruta_id AND "
              . "parada.idparada = detencion.parada_id AND ruta.idruta = '$idruta' ORDER by detencion.orden;"); */
            $consulta = $this->db->query("SELECT * FROM ruta, parada, detencion WHERE parada.idparada = detencion.parada_id AND detencion.ruta_id = ruta.idruta AND ruta.idruta = '$idruta' AND detencion.orden = 1");

            //Devolvemos el resultado de la consulta
            return $consulta->result();
        } else {
            return false;
        }
    }

    public function obtenerRutaCompra($idruta) {

        //Comprobamos si la ruta existe
        $consulta = $this->db->query("SELECT * FROM ruta WHERE ruta.idruta = '$idruta'");
        if ($consulta->num_rows() != 0) {
            //obtenemos la ruta y info relativa
            /* $consulta = $this->db->query("SELECT * FROM ruta, detencion, parada WHERE ruta.idruta = detencion.ruta_id AND "
              . "parada.idparada = detencion.parada_id AND ruta.idruta = '$idruta' ORDER by detencion.orden;"); */
            $consulta = $this->db->query("SELECT * FROM ruta WHERE ruta.idruta = '$idruta'");

            //Devolvemos el resultado de la consulta
            return $consulta->result();
        } else {
            return false;
        }
    }

    public function obtenerParadasRuta($idruta) {

        //Comprobamos si la ruta existe
        $consulta = $this->db->query("SELECT * FROM ruta WHERE ruta.idruta = '$idruta'");
        if ($consulta->num_rows() != 0) {
            //obtenemos la ruta y info relativa
            $consulta = $this->db->query("SELECT * FROM ruta, detencion, parada WHERE ruta.idruta = detencion.ruta_id AND "
                    . "parada.idparada = detencion.parada_id AND ruta.idruta = '$idruta' ORDER by detencion.orden;");

            //Devolvemos el resultado de la consulta
            return $consulta->result();
        } else {
            return false;
        }
    }

    public function actualizarRuta($idruta, $plazas, $duracion, $descripcion) {
        //Comprobamos si la ruta existe
        $consulta = $this->db->query("SELECT * FROM ruta WHERE ruta.idruta = '$idruta'");
        if ($consulta->num_rows() != 0) {
            //actualizamos la ruta
            $consulta = $this->db->query("UPDATE ruta SET plazas = '$plazas', plazas = '$duracion', plazas = '$descripcion'  WHERE (`idruta` = '$idruta');");
            if ($consulta == true) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function actualizarPlazas($idruta, $plazas) {
        //Comprobamos si la ruta existe
        $consulta = $this->db->query("SELECT * FROM ruta WHERE ruta.idruta = '$idruta'");
        if ($consulta->num_rows() != 0) {
            //actualizamos las plazas
            $consulta = $this->db->query("UPDATE ruta SET plazas = '$plazas' WHERE (`idruta` = '$idruta');");
            if ($consulta == true) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function eliminarRuta($idruta) {
        $consulta = $this->db->query("DELETE FROM ruta WHERE idruta=$idruta");
        if ($consulta == true) {
            return true;
        } else {
            return false;
        }
    }

    public function listarRuta() {
        //Hacemos una consulta
        $consulta = $this->db->query("SELECT * FROM ruta;");

        //Devolvemos el resultado de la consulta
        return $consulta->result();
    }

    public function listarRutaPaginaPrincipal() {
        //Hacemos una consulta
        $consulta = $this->db->query("SELECT ruta.idruta 'ruta', 
(SELECT  parada.localidad FROM  detencion, parada WHERE ruta.idruta = detencion.ruta_id AND detencion.tipo_parada = 'origen' AND parada.idparada = detencion.parada_id) 'origen',
 (SELECT  parada.localidad FROM  detencion, parada WHERE ruta.idruta = detencion.ruta_id AND detencion.tipo_parada = 'destino' AND parada.idparada = detencion.parada_id) 'destino', 
ruta.duracion 'duracion', COUNT(detencion.iddetencion) 'paradas'
FROM ruta, detencion, parada 

WHERE ruta.idruta = detencion.ruta_id 
AND detencion.parada_id = parada.idparada

group by ruta.idruta");

        //Devolvemos el resultado de la consulta
        return $consulta->result();
    }

    public function listarRutaFiltro($origen, $destino) {
        //Hacemos una consulta
        $consulta = $this->db->query("SELECT DISTINCT ruta.idruta 'ruta', 
(SELECT DISTINCT parada.localidad FROM detencion, parada WHERE  ruta.idruta = detencion.ruta_id AND detencion.tipo_parada = 'origen' AND parada.idparada = detencion.parada_id LIMIT 1) 'origen', 
(SELECT DISTINCT parada.localidad FROM detencion, parada WHERE ruta.idruta = detencion.ruta_id AND detencion.tipo_parada = 'destino' AND parada.idparada = detencion.parada_id ) 'destino', 
ruta.duracion 'duracion', 
(SELECT COUNT(detencion.iddetencion) FROM detencion, parada WHERE ruta.idruta = detencion.ruta_id AND parada.idparada = detencion.parada_id)  'paradas'
FROM ruta, detencion, parada 
WHERE ruta.idruta = detencion.ruta_id AND parada.idparada = detencion.parada_id AND ((parada.localidad = '$origen' AND detencion.tipo_parada = 'origen') OR (parada.localidad = '$destino' AND detencion.tipo_parada = 'destino'))
group by ruta.idruta
ORDER by detencion.orden");
        //Devolvemos el resultado de la consulta
        return $consulta->result();
    }

    public function obtenerRutaOrigenDestino($idruta) {
        //Hacemos una consulta
        $consulta = $this->db->query("SELECT DISTINCT ruta.idruta 'ruta', 
(SELECT DISTINCT parada.localidad FROM detencion, parada WHERE  ruta.idruta = detencion.ruta_id AND detencion.tipo_parada = 'origen' AND parada.idparada = detencion.parada_id LIMIT 1) 'origen', 
(SELECT DISTINCT parada.localidad FROM detencion, parada WHERE ruta.idruta = detencion.ruta_id AND detencion.tipo_parada = 'destino' AND parada.idparada = detencion.parada_id ) 'destino', 
ruta.duracion 'duracion', 
(SELECT COUNT(detencion.iddetencion) FROM detencion, parada WHERE ruta.idruta = detencion.ruta_id AND parada.idparada = detencion.parada_id)  'paradas'
FROM ruta, detencion, parada 
WHERE ruta.idruta = detencion.ruta_id AND parada.idparada = detencion.parada_id AND ruta.idruta = '$idruta'
group by ruta.idruta
ORDER by detencion.orden");
        //Devolvemos el resultado de la consulta
        return $consulta->result();
    }

    public function valorarRuta($idruta, $valoracion) {

        $valoracionBBDD = 0;
        $nvaloracionesBBDD = 0;

        //Comprobamos si la ruta existe
        $consulta = $this->db->query("SELECT * FROM ruta WHERE ruta.idruta = '$idruta'");
        if ($consulta->num_rows() != 0) {
            //obtenemos nota y numero de valoraciones
            $consulta = $this->db->query("SELECT valoracion, nvaloraciones FROM ruta WHERE ruta.idruta = '$idruta'");
            while ($row = mysql_fetch_assoc($result)) {
                $valoracionBBDD = $row['valoracion'];
                $nvaloracionesBBDD = $row['nvaloraciones'];
            }

            //actualizamos cambios
            $nvaloraciones = $nvaloracionesBBDD + 1;
            $valoracion = (($valoracionBBDD * $nvaloracionesBBDD) + $valoracion) / $nvaloraciones;

            //actualizamos la ruta
            $consulta = $this->db->query("UPDATE ruta SET valoracion = '$valoracion', nvaloraciones = '$nvaloraciones' WHERE (`idruta` = '$idruta');");
            if ($consulta == true) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

}

?>