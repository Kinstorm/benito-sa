<?php

//extendemos CI_Model
class oferta_model extends CI_Model {

    public function __construct() {
        //llamamos al constructor de la clase padre
        parent::__construct();

        //cargamos la base de datos
        $this->load->database();
    }

    public function nuevaOferta($descuento, $fecha_inicio, $fecha_fin, $descripcion, $ruta) {
        $consulta = $this->db->query("INSERT INTO oferta VALUES('$descuento','$fecha_inicio','$fecha_fin', '$descripcion', '$ruta');");
        if ($consulta == true) {
            return true;
        } else {
            return false;
        }
    }

    public function obtenerOferta($idoferta) {

        //Comprobamos si la oferta existe
        $consulta = $this->db->query("SELECT * FROM oferta WHERE oferta.idoferta = '$idoferta'");
        if ($consulta->num_rows() != 0) {
            //obtenemos la oferta y info relativa
            $consulta = $this->db->query("SELECT * FROM oferta WHERE  oferta.idoferta = '$idoferta';");

            //Devolvemos el resultado de la consulta
            return $consulta->result();
        } else {
            return false;
        }
    }
    
        public function obtenerOfertaRuta($idRuta) {

        //Comprobamos si la oferta existe
        $consulta = $this->db->query("SELECT * FROM oferta,ruta WHERE oferta.ruta = ".$idRuta.";");
        if ($consulta->num_rows() != 0) {
            //obtenemos la oferta y info relativa
            $consulta = $this->db->query("SELECT * FROM oferta,ruta WHERE oferta.ruta = ".$idRuta." LIMIT 1;");

            //Devolvemos el resultado de la consulta
            return $consulta->result();
        } else {
            return false;
        }
    }

    public function actualizarOferta($idoferta, $descuento, $fecha_inicio, $fecha_fin, $descripcion, $ruta) {
        //Comprobamos si la oferta existe
        $consulta = $this->db->query("SELECT * FROM oferta WHERE oferta.idoferta = '$idoferta'");
        if ($consulta->num_rows() != 0) {
            //actualizamos la oferta
            $consulta = $this->db->query("UPDATE oferta SET descuento = '$descuento', fecha_inicio = '$fecha_inicio', fecha_fin = '$fecha_fin', "
                    . "descripcion = '$descripcion', ruta = '$ruta' WHERE (`idoferta` = '$idoferta');");
            if ($consulta == true) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function eliminarOferta($idoferta) {
        $consulta = $this->db->query("DELETE FROM oferta WHERE idoferta=$idoferta");
        if ($consulta == true) {
            return true;
        } else {
            return false;
        }
    }

    public function listarOferta() {
        //Hacemos una consulta
        $consulta = $this->db->query("SELECT DISTINCT oferta.idoferta, oferta.descuento, oferta.fecha_inicio, oferta.fecha_fin, oferta.descripcion,
(SELECT DISTINCT parada.localidad FROM detencion, parada WHERE ruta.idruta = detencion.ruta_id AND detencion.tipo_parada = 'origen' AND parada.idparada = detencion.parada_id LIMIT 1) 'origen', 
(SELECT DISTINCT parada.localidad FROM detencion, parada WHERE ruta.idruta = detencion.ruta_id AND detencion.tipo_parada = 'destino' AND parada.idparada = detencion.parada_id LIMIT 1) 'destino'
FROM oferta, ruta, parada, detencion
WHERE oferta.ruta = ruta.idruta
AND ruta.idruta = detencion.ruta_id
AND detencion.parada_id = parada.idparada");

        //Devolvemos el resultado de la consulta
        return $consulta->result();
    }

    public function listarOfertaFiltro($origen, $destino) {
        //Hacemos una consulta
        $consulta = $this->db->query("SELECT DISTINCT oferta.idoferta, oferta.descuento, oferta.fecha_inicio, oferta.fecha_fin, oferta.descripcion, oferta.ruta, 
(SELECT DISTINCT parada.localidad FROM detencion, parada WHERE  ruta.idruta = detencion.ruta_id AND detencion.tipo_parada = 'origen' AND parada.idparada = detencion.parada_id LIMIT 1) 'origen', 
(SELECT DISTINCT parada.localidad FROM detencion, parada WHERE ruta.idruta = detencion.ruta_id AND detencion.tipo_parada = 'destino' AND parada.idparada = detencion.parada_id ) 'destino'

            FROM oferta, ruta, parada, detencion
            WHERE oferta.ruta = ruta.idruta
            AND ruta.idruta = detencion.ruta_id
            AND detencion.parada_id = parada.idparada
            AND  ((parada.localidad = '$origen' AND detencion.tipo_parada = 'origen') OR (parada.localidad = '$destino' AND detencion.tipo_parada = 'destino'))");

        //Devolvemos el resultado de la consulta
        return $consulta->result();
    }

}

?>