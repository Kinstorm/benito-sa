<!DOCTYPE HTML>
<html lang="es">
    <head>
        <meta charset="utf-8"/>
        <title>BenitoSA</title>
        <link rel="icon" href="<?php echo base_url('../images/benitosa.ico'); ?>" type="image/gif">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('../styles/styles.css'); ?>">
    </head>

    <body>
        <div class="container">

            <div class="row header">
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 headerizq ">
                    <div class="superior">
                    </div>
                    <div class="inferior">
                        <button type="button" class="btn btn-info custom" onclick="location.href = '<?php echo base_url(); ?>main_controller/index'">Pantalla anterior</button>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 headermid">
                    <img src="<?php echo base_url('../images/benitosa.png'); ?>" class="img-fluid" alt="Responsive image">
                </div>

                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 headerder ">
                    <div class="superior">
                    </div>
                    <div class="inferior">
                    </div>
                </div>
            </div>

            <div id="cuerpo" class="row" style="display: block;">

                       <h1 style="text-align: center;"> Gestión de viajes</h1>

                <div class="col-lg-2 col-md-2 col-sm-2 hidden-xs leftspace">

                </div>
                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 midspace">  
                    
                    <!--Lista de rutas -->

                    <h4 class="mainrow">Viajes realizados</h4>

                    <table class="table table-hover">
                        <thead>

                            <tr>

                                <td>
                                    <h4>Localizador</h4>
                                </td>
                                <td>
                                    <h4>Asiento</h4>
                                </td>
                                <td>
                                    <h4>Precio</h4>
                                </td>
                                <td>
                                    <h4>Fecha</h4>
                                </td>
                                <td>
                                    <h4>Hora</h4>
                                </td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($listarBilletes as $billete) {
                                ?>
                                <tr> 
                                    <td>
                                        <?= $billete->localizador; ?>
                                    </td>
                                    <td>
                                        <?= $billete->asiento; ?>
                                    </td>
                                    <td>
                                        <?= $billete->precio; ?>
                                    </td>
                                    <td>
                                        <?= $billete->fecha; ?>
                                    </td>
                                     <td>
                                        <?= $billete->hora; ?>
                                    </td>
                                </tr>
                                <?php
                            }
                            ?>
                        </tbody>
                    </table>

                </div>
                <div class=" col-lg-2 col-md-2 col-sm-2 hidden-xs rightspace"></div>
            </div>

            <footer class="row text-center">
                <div class=" col-lg-6 col-md-6 col-sm-6 col-xs-6 footerleft">
                    <button type="button" class="btn btn-info custom" onclick="window.location = '<?php echo base_url("main_controller/cookiesView"); ?>'">Politica de cookies</button>
                </div>
                <div class=" col-lg-6 col-md-6 col-sm-6 col-xs-6 footerright">
                    <button type="button" class="btn btn-info custom" onclick="window.location = '<?php echo base_url("main_controller/politicaView"); ?>'">Avisos legales</button>
                </div>
            </footer>
        </div>
    </body>

</html>