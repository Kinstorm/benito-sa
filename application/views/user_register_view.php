
<!DOCTYPE HTML>
<html lang="es">
    <head>
        <meta charset="utf-8"/>
        <title>BenitoSA</title>
        <link rel="icon" href="<?php echo base_url('../images/benitosa.ico'); ?>" type="image/gif">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('../styles/styles.css'); ?>">
    </head>

    <body>
        <div class="container">
            <div class="row header">
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 headerizq ">
                    <div class="superior">
                    </div>
                    <div class="inferior">
                        <button type="button" class="btn btn-info custom" onclick="location.href = '<?php echo base_url(); ?>main_controller/iniciarSesion'">Pantalla anterior</button>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 headermid">
                     <img src="<?php echo base_url('../images/benitosa.png'); ?>" class="img-fluid" alt="Responsive image">
                </div>

                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 headerder ">
                    <div class="superior">
                    </div>
                    <div class="inferior">

                    </div>
                </div>
            </div>

            <div id="cuerpo" class="row" style="display: block;">

                <form method="post" action="<?php echo base_url() ?>main_controller/registroPost">

                    <h1 style="text-align: center;"> Nuevo usuario</h1>

                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 leftspace">
                        <label for="nombre">Nombre</label><br>
                        <input id="nombre" name="nombre" class="form-control" type="text" title="nombre"  placeholder="nombre" required/>
                        <br><br>

                        <label for="apellidos">Apellidos</label><br>
                        <input id="apellidos" name="apellidos" class="form-control" type="text" title="apellidos"  placeholder="apellidos" required/>
                        <br><br>

                        <label for="correo">Correo Electronico</label><br>
                        <input id="correo" name="correo" class="form-control" type="email" title="correo" placeholder="correo" required/>    
                        <?php if (isset($errorRegister)): ?>
                            <h5> <?php echo $errorRegister ?> </h5>
                        <?php endif; ?>
                        <br><br>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 midspace">

                        <label for="password">Contraseña</label><br>
                        <input id="password" name="password" class="form-control" type="password" title="password"  placeholder="contraseña" required/>
                        <br><br>

                        <label for="dni">DNI</label><br>
                        <input id="dni" name="dni" type="text" class="form-control" title="dni"  placeholder="dni" required/>
                        <br><br>

                        <label for="direccion">Direccion</label><br>
                        <input id="direccion" name="direccion" class="form-control" type="text" title="direccion"  placeholder="direccion" required/>
                        <br><br>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 rightspace">                



                        <label for="telefono">Telefono</label><br>
                        <input id="telefono" name="telefono" class="form-control" type="text" title="telefono"  placeholder="telefono" required/>
                        <br><br>

                        <label for="nacionalidad">Nacionalidad</label><br>
                        <input id="nacionalidad" name="nacionalidad" class="form-control"type="text" title="nacionalidad"  placeholder="nacionalidad" required/>
                        <br><br>

                        <input type="submit"  class="btn btn-info custom" name="submit" value="Enviar" />
                    </div>
                </form>

            </div>


            <footer class="row text-center">
                <div class=" col-lg-6 col-md-6 col-sm-6 col-xs-6 footerleft">
                    <button type="button" class="btn btn-info custom" onclick="window.location = '<?php echo base_url("main_controller/cookiesView"); ?>'">Politica de cookies</button>
                </div>
                <div class=" col-lg-6 col-md-6 col-sm-6 col-xs-6 footerright">
                    <button type="button" class="btn btn-info custom" onclick="window.location = '<?php echo base_url("main_controller/politicaView"); ?>'">Avisos legales</button>
                </div>
            </footer>
        </div>
    </body>

</html>