<!DOCTYPE HTML>
<html lang="es">
    <head>
        <meta charset="utf-8"/>
        <title>BenitoSA</title>
        <link rel="icon" href="<?php echo base_url('../images/benitosa.ico'); ?>" type="image/gif">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('../styles/styles.css'); ?>">
    </head>

    <body>
        <div class="container">

            <div class="row header">
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 headerizq ">
                    <div class="superior">
                    </div>
                    <div class="inferior">
                        <button type="button" class="btn btn-info custom" onclick="location.href = '<?php echo base_url(); ?>main_controller/index'">Pantalla anterior</button>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 headermid">
                    <img src="<?php echo base_url('../images/benitosa.png'); ?>" class="img-fluid" alt="Responsive image">
                </div>

                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 headerder ">
                    <div class="superior">
                    </div>
                    <div class="inferior">
                    </div>
                </div>
            </div>



             <div id="cuerpo" class="row" style="display: block; text-align: center;">

                <h1 style="text-align: center;"> Detalle de oferta</h1>

                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 leftspace">
                    <?php foreach ($oferta_detalle as $fila) { ?>
                        <label for="oferta">Oferta</label><br>
                        <input id="oferta" name="oferta"  class="form-control" type="text" title="oferta"  value="<?php echo $fila->idoferta ?> "  readonly/>
                        <br>

                        <label for="descuento">Descuento</label><br>
                        <input id="descuento" name="descuento"  class="form-control" type="text" title="descuento"  value=" <?php echo $fila->descuento ?> "  readonly/>
                        <br>

                        <label for="fechaini">Fecha inicio</label><br>
                        <input id="fechaini" name="fechaini" class="form-control" type="text" title="fechaini"  value=" <?php echo $fila->fecha_inicio ?> "   readonly/>
                        <br>

                        <label for="fechafin">Fecha final</label><br>
                        <input id="fechafin" name="fechafin" class="form-control" type="text" title="fechafin" value=" <?php echo $fila->fecha_fin ?> "   readonly/>
                        <br>


                    <?php } ?>

                </div>

                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 rightspace">
                    <?php foreach ($oferta_detalle as $fila) { ?>

                        <label for="descripcion">Descripcion</label><br>
                        <textarea id="descripcion" name="descripcion" class="form-control"  rows="5"  readonly><?php echo $fila->descripcion ?></textarea>
                        <br> 

                    <button type="button" class="btn btn-danger" onclick="window.location = '<?php echo base_url("main_controller/adquirirBillete/$fila->ruta"); ?>'">Adquirir billete</button>
                <?php } ?>
                </div>
            </div>

            <footer class="row text-center">
                <div class=" col-lg-6 col-md-6 col-sm-6 col-xs-6 footerleft">
                    <button type="button" class="btn btn-info custom" onclick="window.location = '<?php echo base_url("main_controller/cookiesView"); ?>'">Politica de cookies</button>
                </div>
                <div class=" col-lg-6 col-md-6 col-sm-6 col-xs-6 footerright">
                    <button type="button" class="btn btn-info custom" onclick="window.location = '<?php echo base_url("main_controller/politicaView"); ?>'">Avisos legales</button>
                </div>
            </footer>
        </div>
    </body>

</html>