<!DOCTYPE HTML>
<html lang="es">
    <head>
        <meta charset="utf-8"/>
        <title>BenitoSA</title>
        <link rel="icon" href="<?php echo base_url('../images/benitosa.ico'); ?>" type="image/gif">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('../styles/styles.css'); ?>">
    </head>

    <body>
        <div class="container">

            <div class="row header">
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 headerizq ">
                    <div class="superior">
                    </div>
                    <div class="inferior">
                        <button type="button" class="btn btn-info custom" onclick="location.href = '<?php echo base_url(); ?>main_controller/index'">Pantalla anterior</button>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 headermid">
                    <img src="<?php echo base_url('../images/benitosa.png'); ?>" class="img-fluid" alt="Responsive image">
                </div>

                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 headerder ">
                    <div class="superior">
                    </div>
                    <div class="inferior">
                    </div>
                </div>
            </div>



               <div id="cuerpo" class="row" style="display: block; text-align: center;">

                <h1 style="text-align: center;"> Detalle de ruta</h1>

                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 leftspace">
                    <?php foreach ($ruta_detalle as $fila) { ?>
                        <label for="plazas">Plazas</label><br>
                        <input id="plazas" name="plazas" class="form-control" type="text" title="plazas"  value=" <?php echo $fila->plazas ?> "  readonly/>
                        <br>

                        <label for="duracion">Duracion</label><br>
                        <input id="duracion" name="duracion" class="form-control" type="text" title="duracion"  value=" <?php echo $fila->duracion ?> "  readonly/>
                        <br>

                        <label for="descripcion">Descripcion</label><br>
                        <textarea id="descripcion" name="descripcion" class="form-control"  rows="4"  readonly><?php echo $fila->descripcion ?></textarea>
                        <br>

                        <label for="valoracion">Valoracion</label><br>
                        <input id="valoracion" name="valoracion" class="form-control" type="text" title="valoracion" value=" <?php echo $fila->valoracion ?> " readonly/>
                        <br>

                    <?php } ?>
                </div>


                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 rightspace">

                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <td>
                                    <h4>Localidad</h4>
                                </td>
                                <td>
                                    <h4>Tipo</h4>
                                </td>
                                <td>
                                    <h4>Duracción</h4>
                                </td>
                                <td>
                                    <h4>Hora</h4>
                                </td>
                                <td>
                                    <h4>Direccion</h4>
                                </td>
                            </tr>
                        </thead>
                        <?php
                        foreach ($parada_detalle as $parada) {
                            ?>
                            <tbody class="tablarutas">
                                <tr>

                                    <td>
                                        <?= $parada->localidad; ?>
                                    </td>
                                    <td>
                                        <?= $parada->tipo_parada; ?>
                                    </td>
                                    <td>
                                        <?= $parada->duracion; ?>
                                    </td>
                                    <td>
                                        <?= $parada->hora; ?>
                                    </td>
                                    <td>
                                        <?= $parada->direccion; ?>
                                    </td>
                                </tr>

                            </tbody>
                            <?php
                        }
                        ?>
                    </table> 
                    <?php foreach ($ruta_detalle as $ruta) { ?>
                        <button type="button" class="btn btn-danger" onclick="window.location = '<?php echo base_url("main_controller/adquirirBillete/$ruta->idruta"); ?>'">Adquirir billete</button>
                    <?php } ?>
                </div>

            </div>

            <footer class="row text-center">
                <div class=" col-lg-6 col-md-6 col-sm-6 col-xs-6 footerleft">
                    <button type="button" class="btn btn-info custom" onclick="window.location = '<?php echo base_url("main_controller/cookiesView"); ?>'">Politica de cookies</button>
                </div>
                <div class=" col-lg-6 col-md-6 col-sm-6 col-xs-6 footerright">
                    <button type="button" class="btn btn-info custom" onclick="window.location = '<?php echo base_url("main_controller/politicaView"); ?>'">Avisos legales</button>
                </div>
            </footer>

        </div>
    </body>

</html>