<!DOCTYPE HTML>
<html lang="es">
    <head>
        <meta charset="utf-8"/>
        <title>BenitoSA</title>
        <link rel="icon" href="<?php echo base_url('../images/benitosa.ico'); ?>" type="image/gif">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('../styles/styles.css'); ?>">
    </head>

    <body>
        <div class="container">

            <div class="row header">
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 headerizq ">
                    <div class="superior">
                    </div>
                    <div class="inferior">
                        <button type="button" class="btn btn-info custom" onclick="javascript:window.history.go(-1);">Pantalla anterior</button>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 headermid">
                    <img src="<?php echo base_url('../images/benitosa.png'); ?>" class="img-fluid" alt="Responsive image">
                </div>

                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 headerder ">
                    <div class="superior">
                    </div>
                    <div class="inferior">

                    </div>
                </div>
            </div>

            <div id="cuerpo" class="row" style="display: block; text-align: center;">

                <h1> Compra de billete</h1>

                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12leftspace">      

                    <?php foreach ($rutaDetalle as $fila) { ?>
                        <label for="plazas">Plazas</label><br>
                        <input id="plazas" name="plazas" class="form-control" type="text" title="plazas"  value=" <?php echo $fila->plazas ?> "  readonly/>
                        <br>

                        <label for="duracion">Duracion</label><br>
                        <input id="duracion" name="duracion" class="form-control" type="text" title="duracion"  value=" <?php echo $fila->duracion ?> "  readonly/>
                        <br>

                        <label for="descripcion">Descripcion</label><br>
                        <textarea id="descripcion" name="descripcion" class="form-control"  rows="5"  readonly><?php echo $fila->descripcion ?></textarea>
                        <br>

                        <label for="valoracion">Valoracion</label><br>
                        <input id="valoracion" name="valoracion" class="form-control" type="text" title="valoracion" value=" <?php echo $fila->valoracion ?> " readonly/>
                        <br>

                    <?php } ?>

                </div>
                <div class="col-lg-4 col-md-4 col-sm- col-xs-12 midspace"> 
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <td>
                                    <h4>Localidad</h4>
                                </td>
                                <td>
                                    <h4>Tipo</h4>
                                </td>
                                <td>
                                    <h4>Duracción</h4>
                                </td>
                                <td>
                                    <h4>Hora</h4>
                                </td>
                            </tr>
                        </thead>
                        <?php
                        foreach ($rutaParadas as $parada) {
                            ?>
                            <tbody class="tablarutas">

                                <tr>

                                    <td>
                                        <?= $parada->localidad; ?>
                                    </td>
                                    <td>
                                        <?= $parada->tipo_parada; ?>
                                    </td>
                                    <td>
                                        <?= $parada->duracion; ?>
                                    </td>
                                    <td>
                                        <?= $parada->hora; ?>
                                    </td>
                                </tr>

                            </tbody>
                            <?php
                        }
                        ?>
                    </table> 

                    <?php if (isset($oferta)): ?>

                        <?php foreach ($oferta as $fila) { ?>

                            <label for="descuento">Oferta actual</label><br>
                            <input id="descuento" name="descuento"  class="form-control" type="text" title="descuento"  value=" <?php echo $fila->descuento ?>%"  readonly/>
                            <br>
                        <?php } ?>
                    <?php else: ?>
                        <label for="descuento">Oferta actual</label><br>
                        <input id="descuento" name="descuento"  class="form-control" type="text" title="descuento"  value="0%"  readonly/>
                        <br>
                    <?php endif; ?>



                </div>
                <div class=" col-lg-4 col-md-4 col-sm-4 col-xs-12 rightspace">
                    <?php foreach ($usuario as $fila) { ?>
                        <label for="nombre">Nombre</label><br>
                        <input id="nombre" name="nombre" class="form-control" type="text" title="nombre"  value=" <?php echo $fila->nombre ?> "  readonly/>
                        <br>

                        <label for="apellidos">Apellidos</label><br>
                        <input id="apellidos" name="apellidos" class="form-control" type="text" title="apellidos"  value=" <?php echo $fila->apellidos ?> "  readonly/>
                        <br>

                        <label for="email">Correo</label><br>
                        <input id="email" name="email" class="form-control" type="text" title="email"  value=" <?php echo $fila->email ?> "  readonly/>
                        <br>

                        <label for="telefono">Telefono</label><br>
                        <input id="telefono" name="telefono" class="form-control" type="text" title="telefono" value=" <?php echo $fila->telefono ?> " readonly/>
                        <br>

                    <?php } ?>

                    <?php foreach ($rutaDetalle as $filarut) { ?>

                        <button type="button" class="btn btn-danger" onclick="window.location = '<?php echo base_url("main_controller/adquirirBilleteCompra/$filarut->idruta"); ?>'">Adquirir billete</button>

                    <?php } ?>

                </div>
            </div>

            <footer class="row text-center">
                <div class=" col-lg-6 col-md-6 col-sm-6 col-xs-6 footerleft">
                    <button type="button" class="btn btn-info custom" onclick="window.location = '<?php echo base_url("main_controller/cookiesView"); ?>'">Politica de cookies</button>
                </div>
                <div class=" col-lg-6 col-md-6 col-sm-6 col-xs-6 footerright">
                    <button type="button" class="btn btn-info custom" onclick="window.location = '<?php echo base_url("main_controller/politicaView"); ?>'">Avisos legales</button>
                </div>
            </footer>
        </div>
    </body>

</html>