<!DOCTYPE HTML>
<html lang="es">
    <head>
        <meta charset="utf-8"/>
        <title>BenitoSA</title>
        <link rel="icon" href="<?php echo base_url('../images/benitosa.ico'); ?>" type="image/gif">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('../styles/styles.css'); ?>">
    </head>

    <body>
        <div class="container">

            <div class="row header">
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 headerizq ">
                    <div class="superior">
                    </div>
                    <div class="inferior">
                        <button type="button" class="btn btn-info custom" onclick="location.href = '<?php echo base_url(); ?>main_controller/index'">Pantalla anterior</button>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 headermid">
                    <img src="<?php echo base_url('../images/benitosa.png'); ?>" class="img-fluid" alt="Responsive image">
                </div>

                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 headerder ">
                    <div class="superior">
                    </div>
                    <div class="inferior">
                        <button type="button" class="btn btn-info custom" onclick="window.location = '<?php echo base_url("main_controller/registro"); ?>'">Nuevo usuario</button>
                    </div>
                </div>
            </div>

            <div id="cuerpo" class="row" style="display: block;">


                <div class="col-lg-4 col-md-4 col-sm-3 hidden-xs leftspace">

                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 midspace">                


                    <h1> Iniciar sesión </h1>

                    <form method="post" action="<?php echo base_url() ?>main_controller/iniciarSesionPost">
                        <label for="correo">Correo Electronico</label><br>
                        <input id="correo" name="correo" class="form-control" type="email" title="correo" placeholder="correo" required/>
                        <br><br>


                        <label for="password">Contraseña</label><br>
                        <input id="password" name="password" class="form-control" type="password" title="password"  placeholder="Contraseña" required/>
                        <br><br>

                        <input type="submit" name="submit" class="btn btn-info custom" value="Iniciar sesión" />

                        <?php if (isset($errorLogin)): ?>
                            <h5> <?php echo $errorLogin; ?> </h5>
                        <?php endif; ?>
                    </form>





                </div>
                <div class=" col-lg-4 col-md-4 col-sm-3 hidden-xs rightspace"></div>
            </div>

            <footer class="row text-center">
                <div class=" col-lg-6 col-md-6 col-sm-6 col-xs-6 footerleft">
                    <button type="button" class="btn btn-info custom" onclick="window.location = '<?php echo base_url("main_controller/cookiesView"); ?>'">Politica de cookies</button>
                </div>
                <div class=" col-lg-6 col-md-6 col-sm-6 col-xs-6 footerright">
                    <button type="button" class="btn btn-info custom" onclick="window.location = '<?php echo base_url("main_controller/politicaView"); ?>'">Avisos legales</button>
                </div>
            </footer>
        </div>
    </body>

</html>