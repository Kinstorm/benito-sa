<!DOCTYPE HTML>
<html lang="es">
    <head>
        <meta charset="utf-8"/>
        <title>BenitoSA</title>
        <link rel="icon" href="<?php echo base_url('../images/benitosa.ico'); ?>" type="image/gif">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('../styles/styles.css'); ?>">
    </head>

    <body>
        <div class="container">
            <div class="row header">
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 headerizq ">
                    <div class="superior">
                        <?php if (isset($nombre)): ?>
                            <h5>Bienvenido/a <?php echo $nombre ?></h5>
                        <?php endif; ?>
                    </div>
                    <div class="inferior">
                        <?php if (isset($nombre)): ?>

                            <button type="button" class="btn btn-danger custom" onclick="window.location = '<?php echo base_url("main_controller/cerrarSesion"); ?>'">Cerrar sesion</button>

                        <?php else: ?>
                            <button type="button" class="btn btn-success custom" onclick="window.location = '<?php echo base_url("main_controller/iniciarSesion"); ?>'">Login</button>
                        <?php endif; ?>
                    </div>

                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 headermid">
                    <img  <img src="<?php echo base_url('../images/benitosa.png'); ?>" class="img-fluid" alt="Responsive image">
                </div>

                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 headerder ">

                    <div class="superior">

                    </div>

                    <div class="inferior">
                        <?php if (isset($nombre)): ?>
                            <button type="button" class="btn btn-info custom" onclick="window.location = '<?php echo base_url("main_controller/misViajes"); ?>'">Mis viajes</button>
                        <?php else: ?>
                        <?php endif; ?>
                    </div>

                </div>


            </div>

            <div id="cuerpo" class="row">
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 leftspace">
                    <!--Lista de rutas -->

                    <h4 class="mainrow">Rutas activas</h4>

                    <table class="table table-hover">
                        <thead>

                            <tr>

                                <td>
                                    <h4>Origen</h4>
                                </td>
                                <td>
                                    <h4>Destino</h4>
                                </td>
                                <td>
                                    <h4>Duracion</h4>
                                </td>
                                <td>
                                    <h4>Paradas</h4>
                                </td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($listarRuta as $ruta) {
                                ?>
                                <tr onclick="window.location = '<?= base_url("main_controller/detalleRuta/$ruta->ruta") ?>';">
                                    <td>
                                        <?= $ruta->origen; ?>
                                    </td>
                                    <td>
                                        <?= $ruta->destino; ?>
                                    </td>
                                    <td>
                                        <?= $ruta->duracion; ?>
                                    </td>
                                    <td>
                                        <?= $ruta->paradas; ?>
                                    </td>
                                </tr>
                                <?php
                            }
                            ?>
                        </tbody>
                    </table>

                </div>

                <div class="col-lg-5 col-md-6 col-sm-12 col-xs-12 midspace" >
                    <!--Lista de ofertas -->

                    <table class="table table-hover">
                        <thead class="thead-light">
                        <h4 class="mainrow">Ofertas disponibles</h4>
                        <tr>
                            <td>
                                <h4>Descuento</h4>
                            </td>
                            <td>
                                <h4>Inicio</h4>
                            </td>
                            <td>
                                <h4>Fin</h4>
                            </td>
                            <td COLSPAN="2">
                                <h4>Ruta</h4>
                            </td>
                        </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($listarOferta as $oferta) {
                                ?>
                                <tr  onclick="window.location = '<?= base_url("main_controller/detalleOferta/$oferta->idoferta") ?>';">
                                    <td>
                                        <?= $oferta->descuento . "%"; ?>
                                    </td>
                                    <td>
                                        <?= $oferta->fecha_inicio; ?>
                                    </td>
                                    <td>
                                        <?= $oferta->fecha_fin; ?>
                                    </td>
                                    <td >
                                        <?= $oferta->origen; ?>
                                    </td>
                                    <td >
                                        <?= $oferta->destino; ?>
                                    </td>
                                </tr>
                                <?php
                            }
                            ?>
                        </tbody>
                    </table>

                </div>
                <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 rightspace">
                    <div class="form-group">

                        <!-- formulario busqueda rutas-->
                        <form action=" <?= base_url("main_controller/filtrado"); ?>" method="post">
                            <label for="origen">Origen</label><br>
                            <?php if (isset($origen)): ?>
                                <input class="form-control" id="origen" name="origen" type="text" title="origen" placeholder="Estacion de origen" value=" <?= $origen; ?>" required/>
                            <?php else: ?>                                                                                   
                                <input class="form-control" id="origen" name="origen" type="text" title="origen" placeholder="Estacion de origen" required/>
                            <?php endif; ?>
                            <br><br>

                            <label for="destino">Destino</label><br>
                            <?php if (isset($destino)): ?>
                                <input id="destino"  class="form-control" name="destino" type="text" title="destino"  placeholder="Estacion de destino"  value=" <?= $destino; ?>" required/>
                            <?php else: ?>                                                                                   
                                <input id="destino"  class="form-control" name="destino" type="text" title="destino"  placeholder="Estacion de destino" required/>
                            <?php endif; ?>
                            <br><br>

                            <label for="pasajeros">Pasajeros</label><br>
                            <?php if (isset($pasajeros)): ?>
                                <input id="pasajeros" class="form-control" name="pasajeros" type="text" title="pasajeros"  value=" <?= $pasajeros; ?>"  required/>
                            <?php else: ?>                                                                                   
                                <input id="pasajeros" class="form-control" name="pasajeros" type="number" title="pasajeros"  placeholder="numero de pasajeros" required/>
                            <?php endif; ?>
                            <br><br>

                            <input type="submit"  class="btn btn-default" name="submit" onclick="window.location = '<?php echo base_url("main_controller/filtrado"); ?>'" value="Buscar" />
                            <input class="btn btn-default" name="submit"  type="reset" onclick="window.location = '<?php echo base_url("main_controller/index"); ?>'" value="Resetear búsqueda" />    
                        </form>
                    </div>
                </div>
            </div>
            <footer class="row text-center">
                <div class=" col-lg-6 col-md-6 col-sm-6 col-xs-6 footerleft">
                    <button type="button" class="btn btn-info custom" onclick="window.location = '<?php echo base_url("main_controller/cookiesView"); ?>'">Politica de cookies</button>
                </div>
                <div class=" col-lg-6 col-md-6 col-sm-6 col-xs-6 footerright">
                    <button type="button" class="btn btn-info custom" onclick="window.location = '<?php echo base_url("main_controller/politicaView"); ?>'">Avisos legales</button>
                </div>

            </footer>
        </div>
    </body>

</html>