<!DOCTYPE HTML>
<html lang="es">
    <head>
        <meta charset="utf-8"/>
        <title>BenitoSA</title>
        <link rel="icon" href="<?php echo base_url('../images/benitosa.ico'); ?>" type="image/gif">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('../styles/styles.css'); ?>">
    </head>

    <body>
        <div class="container">

            <div class="row header">
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 headerizq ">
                    <div class="superior">
                        <?php if (isset($nombre)): ?>
                            <h5>Bienvenido/a <?php echo $nombre ?></h5>
                        <?php endif; ?>
                    </div>
                    <div class="inferior">
                        <?php if (isset($nombre)): ?>

                            <button type="button" class="btn btn-danger custom" onclick="window.location = '<?php echo base_url("main_controller/cerrarSesion"); ?>'">Cerrar sesion</button>

                        <?php else: ?>
                            <button type="button" class="btn btn-success custom" onclick="window.location = '<?php echo base_url("main_controller/iniciarSesion"); ?>'">Login</button>
                        <?php endif; ?>
                    </div>

                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 headermid">
                    <img  <img src="<?php echo base_url('../images/benitosa.png'); ?>" class="img-fluid" alt="Responsive image">
                </div>

                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 headerder ">

                    <div class="superior">

                    </div>

                    <div class="inferior">

                    </div>

                </div>


            </div>



            <div id="cuerpo" class="row" style="display: block;">

                <h1 style="text-align: center;">Panel de administración</h1>
                
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 leftspace">
                    <button type="button" class="btn btn-info custom" style="margin-top: 15vh; height: 20vh;  border-radius: 25px;" onclick="window.location = '<?php echo base_url("main_controller/cookiesView"); ?>'">Rutas</button>

                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 midspace">                
                    <button type="button" class="btn btn-info custom" style="margin-top: 15vh; height: 20vh; border-radius: 25px; "  onclick="window.location = '<?php echo base_url("main_controller/cookiesView"); ?>'">Ofetas</button>

                </div>
                <div class=" col-lg-4 col-md-4 col-sm-4 col-xs-12 rightspace">
                    <button type="button" class="btn btn-info custom" style="margin-top: 15vh; height: 20vh; border-radius: 25px;" onclick="window.location = '<?php echo base_url("main_controller/cookiesView"); ?>'">Paradas</button>


                </div>
            </div>

            <footer class="row text-center">
                <div class=" col-lg-6 col-md-6 col-sm-6 col-xs-6 footerleft">
                    <button type="button" class="btn btn-info custom" onclick="window.location = '<?php echo base_url("main_controller/cookiesView"); ?>'">Politica de cookies</button>
                </div>
                <div class=" col-lg-6 col-md-6 col-sm-6 col-xs-6 footerright">
                    <button type="button" class="btn btn-info custom"  onclick="window.location = '<?php echo base_url("main_controller/politicaView"); ?>'">Avisos legales</button>
                </div>
            </footer>
        </div>
    </body>

</html>