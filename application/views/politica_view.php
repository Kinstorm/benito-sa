<!DOCTYPE HTML>
<html lang="es">
    <head>
        <meta charset="utf-8"/>
        <title>BenitoSA</title>
        <link rel="icon" href="<?php echo base_url('../images/benitosa.ico'); ?>" type="image/gif">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('../styles/styles.css'); ?>">
    </head>

    <body>
        <div class="container">

            <div class="row header">
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 headerizq ">
                    <div class="superior">
                    </div>
                    <div class="inferior">
                        <button type="button" class="btn btn-info custom" onclick="location.href = '<?php echo base_url(); ?>main_controller/index'">Pantalla anterior</button>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 headermid">
                    <img src="<?php echo base_url('../images/benitosa.png'); ?>" class="img-fluid" alt="Responsive image">
                </div>

                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 headerder ">
                    <div class="superior">
                    </div>
                    <div class="inferior">
                    </div>
                </div>
            </div>



            <div id="cuerpo" class="row" style="display: block;">

                <h1 style="text-align: center;">Avisos legales</h1>


                <div class="col-lg-2 col-md-2 col-sm-1 hidden-xs leftspace">

                </div>
                <div class="col-lg-8 col-md-8 col-sm-10 col-xs-12 midspace texto">                


                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam molestie vitae mi sed blandit. Pellentesque id pellentesque nunc. Quisque condimentum, nisi quis pharetra porttitor, nulla quam pharetra lorem, in imperdiet tellus metus non justo. Integer lorem nulla, tincidunt in massa vitae, pellentesque tincidunt augue. Cras sodales placerat nisi et lobortis. Donec ut suscipit nisi. Curabitur non scelerisque tellus, a semper nisi.

                        Mauris eget ante ac risus lacinia vehicula. Nam tempus elit in nisi tincidunt mattis. Integer finibus leo sem, in lacinia erat dapibus a. Nulla tincidunt auctor nisi, finibus laoreet ex lacinia sed. Duis rhoncus tincidunt lacus eu blandit. Integer vel vestibulum felis. Quisque tincidunt odio sit amet fermentum vulputate. Pellentesque vulputate ipsum tempus dapibus tincidunt. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec aliquet consectetur tellus in tincidunt. Nulla lacus sapien, mattis ac euismod eu, venenatis at justo. Donec nec tristique arcu, quis bibendum tellus. Vivamus id ex massa. Nullam a eros nisi.

                        Fusce convallis dignissim enim, quis tincidunt elit dignissim et. Sed ut lacus eget mauris suscipit consectetur consectetur ut erat. Vivamus auctor eros in turpis condimentum, ac volutpat justo volutpat. Nunc ac venenatis dolor. Pellentesque faucibus porttitor diam id vehicula. Duis viverra tortor purus, eget rhoncus lacus tincidunt ac. Sed sodales magna ut sem posuere hendrerit. Vivamus facilisis condimentum tellus vel sollicitudin. Suspendisse potenti. Aliquam erat volutpat. Sed eget nisl vitae ligula bibendum euismod. Integer pharetra tellus metus, nec tincidunt enim maximus id. Sed aliquet a est nec venenatis.
                    </p>

                </div>
                <div class=" col-lg-2 col-md-2 col-sm-1 hidden-xs rightspace"></div>



            </div>

            <footer class="row text-center">
                <div class=" col-lg-6 col-md-6 col-sm-6 col-xs-6 footerleft">
                    <button type="button" class="btn btn-info custom" onclick="window.location = '<?php echo base_url("main_controller/cookiesView"); ?>'">Politica de cookies</button>
                </div>
                <div class=" col-lg-6 col-md-6 col-sm-6 col-xs-6 footerright">
                    <button type="button" class="btn btn-info custom" onclick="window.location = '<?php echo base_url("main_controller/politicaView"); ?>'">Avisos legales</button>
                </div>
            </footer>

        </div>
    </body>

</html>